SSH_SERVER='uapp@45.67.59.246'
REGISTRY_NAME='registry.gitlab.com/untypical/app/landing'
COMPOSE_DIR='~/compose/production'

START_TIME=$SECONDS

STATUS=$(git status)
if [[ $STATUS != *"nothing to commit, working tree clean"* || $STATUS == *"Changes not staged for commit"* ]]; then
  echo "Please, push all changes!"
  exit 1
fi

APP_VERSION=$(git describe --tags --abbrev=0)
IFS='.' read -ra v_arr <<< "${APP_VERSION:1}"
case "$1" in
  "major")
    APP_VERSION=$((${v_arr[0]}+1)).0.0
    ;;
  "minor")
    APP_VERSION=${v_arr[0]}.$((${v_arr[1]}+1)).0
    ;;
  "patch")
    APP_VERSION=${v_arr[0]}.${v_arr[1]}.$((${v_arr[2]}+1))
    ;;
  *)
    APP_VERSION="$1"
    ;;
esac

COMMIT_DESC="$2"

docker build . -t $REGISTRY_NAME:$APP_VERSION && docker push $REGISTRY_NAME:$APP_VERSION &&
docker tag $REGISTRY_NAME:$APP_VERSION $REGISTRY_NAME:latest && docker push $REGISTRY_NAME:latest &&
ssh $SSH_SERVER "cd $COMPOSE_DIR && docker-compose pull && docker-compose up -d" && git reset --hard HEAD

DURATION=$(($SECONDS - $START_TIME))
echo production deploy finished at $((DURATION / 60))m $((DURATION % 60))s
