import px2vw from '../utils/px2vw'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {AppleIcon, AndroidIcon, WebIcon} from '.'

const Platforms = (props) => {
    const {container, h3, iconsContainer, icon} = useStyles();
    const theme = useTheme();

    return (
        <div className={container}>
            <div className={iconsContainer}>
                <AppleIcon fill={theme.palette.text.primary} className={icon}/>
                <AndroidIcon fill={theme.palette.text.primary} className={icon}/>
                <WebIcon fill={theme.palette.text.primary} className={icon}/>
            </div>
            <h3 className={h3}>iOS, Android приложения<br/>и web‑сервисы</h3>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        [theme.breakpoints.down('xs')]: {
            marginBottom: 15,
        },
    },
    iconsContainer: {
        flexDirection: 'row',
    },
    h3: {
        margin: 0,
        marginTop: 10,
        fontWeight: 400,
        fontSize: 20,
        whiteSpace: 'nowrap',
        [theme.breakpoints.down('sm')]: {
            fontSize: 18,
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: 16,
            marginTop: 5,
        }
    },
    icon: {
        marginRight: 15,
        width: 27,
        [theme.breakpoints.down('xs')]: {
            width: 24,
        }
    }
}));

export default Platforms;
