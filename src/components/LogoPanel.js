import { makeStyles } from '@material-ui/core/styles';
import {useHistory} from "react-router-dom";

const LogoPanel = (props) => {
    const {color} = props;
    const {h1} = useStyles();

    let history = useHistory();

    const onLinkTo = (path) => () => {
        history.push(path)
    }

    return (
        <h1 onClick={onLinkTo('/')} className={h1} style={{color}}>Untypical<br/>Company</h1>
    );
}

const useStyles = makeStyles((theme) => ({
    h1: {
        margin: 0,
        fontSize: 20,
        fontWeight: 'bold',
        cursor: 'pointer'
    }
}));

export default LogoPanel;
