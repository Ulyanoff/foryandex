import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import {StatPlatforms, StatScreens, BlockHeader} from '.'

const BlockPortfolio = (props) => {
    const {title, textColor, subTitle, isIOS, isAndroid, isWeb, screens, texts, image, 
        bigPhoneBackgroundColor, isDarkTheme, onDarkThemeChange} = props;
    const {root,container} = useStyles();

    return (
        <div className={root}>
            <BlockHeader color={textColor} isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
            <div className={container}>
                <div style={{flexGrow: 1}}>
                    <Titles color={textColor} title={title} subTitle={subTitle} isIOS={isIOS} isAndroid={isAndroid} isWeb={isWeb} screens={screens}/>
                    <About texts={texts}/>
                </div>
                <Box display={{  xs: 'none', sm: 'block'}}>
                    <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start'}}>
                        <BigPhone image={image} backgroundColor={bigPhoneBackgroundColor}/>
                    </div>
                </Box>
            </div>
        </div>
    );
}

const Titles = (props) => {
    const {title, subTitle, isIOS, isAndroid, isWeb, screens, color} = props;
    const {titlesContainer, h1, h2} = useStyles();

    return (
        <div className={titlesContainer}>
            <div style={{width: '100%'}}>
                <h1 className={h1} style={{color}}>{title}</h1>
                <h2 className={h2} style={{color}}>{subTitle}</h2>
            </div>
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'flex-end', marginTop: 50}}>
                <StatPlatforms isIOS={isIOS} isAndroid={isAndroid} isWeb={isWeb} color={color}/>
                <div style={{width: 50}}></div>
                <StatScreens text={screens} color={color}/>
            </div>
        </div>
    )
}

const About = (props) => {
    const {texts} = props;
    const {ul, li} = useStyles();

    return (
        <div style={{marginTop: 100, marginLeft: 0, marginRight: 50}}>
            <ul className={ul}>
                {texts && texts.map((text) => 
                    <li className={li}>{text}</li>
                )}
            </ul>
        </div>
    )
}

const BigPhone = (props) => {
    const {image, backgroundColor} = props;
    const {bigPhone, bigScreen} = useStyles();

    return (
        <div className={bigPhone} style={{backgroundColor: backgroundColor?backgroundColor:'white', }}>
            <img src={image} className={bigScreen}></img>
            <div style={{backgroundColor: 'gray', height: 4, width: '30%', marginTop: 30, marginBottom: 15, borderRadius: 2}}/>
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
    },
    container: {
        display: 'flex', 
        flexDirection: 'row',
        paddingTop: 100,
        [theme.breakpoints.up('md')]: {
            paddingTop: 160,
        },
    },
    h1: {
        fontWeight: 'bold',
        margin: 0,
        fontSize: 60,
        [theme.breakpoints.down('sm')]: {
            fontSize: 40
        },
        // [theme.breakpoints.down('xs')]: {
        //     fontSize: 25
        // }
    },
    h2: {
        fontWeight: 400,
        margin: 0,
        marginTop: 10,
        fontSize: 23,
        wordBreak:'keep-all',
        [theme.breakpoints.down('sm')]: {
            fontSize: 20,
        },
        // [theme.breakpoints.down('xs')]: {
        //     fontSize: 18,
        // }
    },
    bigPhone: {
        borderRadius: 40,
        width: 360,
        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.4)',
        overflow: 'hidden',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        [theme.breakpoints.down('md')]: {
            borderRadius: 30,
            width: 300,
        },
    },
    bigScreen: {
        width: '100%',
        height: 'auto',
        maxHeight: 630,
    },
    ul: {
        width: '100%',
        paddingLeft: 20,
        fontSize: 20
    },
    li: {
        marginBottom: 20
    },
    titlesContainer: {
        height: 400, 
        display: 'flex', 
        flexDirection: 'column', 
        alignItems: 'flex-start', 
        justifyContent: 'space-between',
        [theme.breakpoints.up('md')]: {
            height: 440,
        },
        [theme.breakpoints.up('sm')]: {
            marginRight: 50,
        },
    }
}));

export default BlockPortfolio;
