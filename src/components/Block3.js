
import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {useHistory} from "react-router-dom";

import {PortfolioItem} from '.'

import bookcrosserscreen from '../assets/img/bookcrosserscreen1.png'
import vdelescreen from '../assets/img/vdelescreen1.png'
import involverscreen from '../assets/img/involverscreen1.png'

const Block3 = (props) => {
    const {root, icon} = useStyles();

    let history = useHistory();

    const onLinkTo = (path) => () => {
        history.push(path)
    }

    return (
        <div className={root}>
            <Grid container spacing={6}>
                <Grid item xs={12} md={6}>
                    <PortfolioItem onClick={onLinkTo('/vdele')} title={'VDele'} color='white' backgroundColor='#22262E' isIOS isAndroid isWeb backgroundImage={vdelescreen}/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <PortfolioItem onClick={onLinkTo('/involver')} title={'Involver'} color='black' backgroundColor='#FFFFFF' isIOS isAndroid isWeb backgroundImage={involverscreen}/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <PortfolioItem onClick={onLinkTo('/bookcrosser')} title={'BookCrosser'} color='white' backgroundColor='#DC0000' isIOS isAndroid backgroundImage={bookcrosserscreen}/>
                </Grid>
            </Grid>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 80,
    },
    icon: {
        marginRight: 15,
        width: 27,
        [theme.breakpoints.down('xs')]: {
            width: 24,
        }
    }
}));

export default Block3;
