
import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import {AppleIcon, AndroidIcon, WebIcon} from '.'

const PortfolioItem = (props) => {
    const {onClick, title, color, backgroundColor, isAndroid, isIOS, isWeb, backgroundImage} = props;
    const {root, icon, image} = useStyles();
    const [isHovered, setIsHovered] = useState(false);

    const onEnter = () => {
        setIsHovered(true)
    }

    const onLeave = () => {
        setIsHovered(false)
    }

    return (
        <Paper onClick={onClick} className={root} onMouseEnter={onEnter} onMouseLeave={onLeave} style={{backgroundColor: backgroundColor, boxShadow: isHovered?'0px 0px 30px rgba(0,0,0,0.3)':'0px 0px 6px rgba(0,0,0,0.16)'}}>
            <Grid container>
                <Grid item xs style={{fontSize: 30, fontWeight: 'bold', color: color, paddingRight: 10}}>{title}</Grid>
                <Grid item style={{display: 'flex', alignItems: 'center'}}>
                    <div style={{whiteSpace: 'nowrap'}}>
                        {isIOS && <AppleIcon fill={color} className={icon}/>}
                        {isAndroid && <AndroidIcon fill={color} className={icon}/>}
                        {isWeb && <WebIcon fill={color} className={icon}/>}
                    </div>
                </Grid>
            </Grid>
            <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center', paddingTop: 50}}>
                <div className={image} style={{backgroundImage: `url(${backgroundImage})`, bottom: isHovered?0:-10}}></div>
            </div>
        </Paper>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative', 
        width: '100%', 
        height: 400, 
        padding: 30, 
        paddingBottom: 0, 
        overflow: 'hidden', 
        cursor: 'pointer'
    },
    icon: {
        marginRight: 15,
        width: 27,
        [theme.breakpoints.down('xs')]: {
            width: 24,
        }
    },
    image: {
        backgroundPosition: 'top', 
        backgroundSize: 'cover', 
        backgroundRepeat: 'no-repeat', 
        backgroundColor: 'white', 
        borderRadius: '20px 20px 0px 0px',
        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.4)',
        position: 'absolute',
        width: '60%', 
        height: 280,
        transition: 'all 0.1s ease-in'
    }
}));

export default PortfolioItem;
