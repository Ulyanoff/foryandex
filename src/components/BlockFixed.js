import React, {useState} from "react";
import {BlockFixedHeader, ContactButton, LogoPanel, DarkSwitch} from '.'
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const BlockFixed = (props) => {
    const {isDarkTheme, onDarkThemeChange, onContactClick} = props;
    const {container, button, h1} = useStyles();

    return (
        <Box display={{ xs: 'none', md: 'block' }}>
            <BlockFixedHeader style={{left: 0}}>
                <LogoPanel/>
            </BlockFixedHeader>
            <BlockFixedHeader style={{right: 0}}>
                <ContactButton onClick={onContactClick}/>
                <DarkSwitch checked={isDarkTheme} onChange={onDarkThemeChange} name="checkedA" />
            </BlockFixedHeader>
        </Box>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
    },
}));

export default BlockFixed;
