
import React from "react";
import { BlockHeader, Block1Header, Block1Footer } from ".";
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const Block1 = (props) => {
    const {isDarkTheme, onDarkThemeChange} = props;
    const {container} = useStyles();

    return (
        <div className={container}>
            <BlockHeader isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
            <Block1Header/>
            <Block1Footer/>
        </div>

    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        [theme.breakpoints.up('lg')]: {
            height: '100vh',
            maxHeight: 900,
        },
        [theme.breakpoints.only('sm')]: {
            // minHeight: '100vh',
            // maxHeight: 700,
        },
        [theme.breakpoints.down('xs')]: {
            minHeight: '100vh',
        }
    }
}));

export default Block1;
