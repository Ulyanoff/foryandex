import React from "react";
import {Platforms} from '.'
import block1Img from '../assets/img/pixelcoder.gif'
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import { makeStyles } from '@material-ui/core/styles';

const Block1Footer = (props) => {
    const {container, mobile, desktop, image} = useStyles();

    return (
        <div>
            <Hidden mdUp>
                <div className={mobile}>
                    <div className={image}/>
                    <Platforms/>
                </div>
            </Hidden>
            <Hidden smDown>
                <div className={desktop}>
                    <Platforms/>
                    <div className={image}/>
                </div>
            </Hidden>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        justifyContent: "center",
        alignItems: "stretch"
    },
    mobile: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    desktop: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "flex-end"
    },
    image: {
        width: '100%', 
        [theme.breakpoints.up('md')]: {
            height: 350,
        },
        [theme.breakpoints.only('sm')]: {
            height: 275,
        },
        [theme.breakpoints.down('xs')]: {
            height: 250,
        },
        marginTop: 15,
        marginBottom: 15,
        backgroundPosition: 'right',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundImage: `url("${block1Img}")`
    }
}));

export default Block1Footer;
