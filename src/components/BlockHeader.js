import React, {useState} from "react";
import {LogoPanel, DarkSwitch} from '.'
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const BlockHeader = (props) => {
    const {isDarkTheme, onDarkThemeChange, color} = props;
    const {container} = useStyles();

    return (
        <Box display={{ xs: 'block', md: 'none' }}>
            <div className={container}>
                <LogoPanel color={color}/>
                <DarkSwitch color={color} checked={isDarkTheme} onChange={onDarkThemeChange} name="checkedA" />
            </div>
        </Box>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        marginTop: 30,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-center'
    }
}));

export default BlockHeader;
