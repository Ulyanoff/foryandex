import { withStyles, makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

const USwitch = withStyles((theme) => ({
    root: {
      width: 60,
      height: 26,
      padding: 0,
      display: 'flex',
      overflow: 'visible'
    },
    switchBase: {
      padding: 3,
      color: '#FFE762',
      '&$checked': {
        transform: 'translateX(34px)',
        color: theme.palette.background.default,
        '& + $track': {
          opacity: 1,
          backgroundColor: '#FFE762',
        },
      },
    },
    thumb: {
      width: 20,
      height: 20,
      boxShadow: 'none',
    },
    track: {
        borderRadius: 13,
        opacity: 1,
        backgroundColor: '#101010',
        boxShadow: '0px 3px 6px rgba(0,0,0,0.16)'
    },
    checked: {},
  }))(Switch);

const DarkSwitch = (props) => {
  const {color} = props;
  const {container, h3} = useStyles();

  return (
    <div className={container}>
      <USwitch {...props} />
      <h3 className={h3} style={{color}}>Темная тема</h3>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  h3: {
    fontSize: 12,
    fontWeight: 400
  },
}));

export default DarkSwitch;
