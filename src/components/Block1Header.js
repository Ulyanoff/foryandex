
import React, { useState, useEffect } from "react";
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import px2vw from '../utils/px2vw'

const words = ["нетипичные", "масштабные", "качественные", "крутые"];

const Block1Header = (props) => {
    const {container, h1, h2} = useStyles();

    const [index, setIndex] = useState(0);
    const [subIndex, setSubIndex] = useState(0);
    const [blink, setBlink] = useState(true);
    const [reverse, setReverse] = useState(false);

    useEffect(() => {
        if(words){
            if ( subIndex === words[index].length + 1 && !reverse ) {
                setReverse(true);
                return;
            }
        
            if (subIndex === 0 && reverse) {
                setReverse(false);
                if(index === words.length-1){
                    setIndex(0)
                }else{
                    setIndex((prevIndex) => prevIndex + 1);
                }
                return;
            }
        
            const timeout = setTimeout(() => {
                setSubIndex((prev) => prev + (reverse ? -1 : 1));
            }, reverse ? 75 : Math.max(subIndex === words[index].length ? 4000 :
                150, parseInt(Math.random() * 350)));
        
            return () => clearTimeout(timeout);
        }
    }, [subIndex, index, reverse]);

    // blinker
    useEffect(() => {
        const timeout2 = setTimeout(() => {
        setBlink((prevBlink) => !prevBlink);
        }, 800);
        return () => clearTimeout(timeout2);
    }, [blink]);

    return (
        <div className={container}>
            <h1 className={h1}>Студия разработки<br/>мобильных экосистем</h1>
            {words && <h2 className={h2}>Создаем {`${words[index].substring(0, subIndex)}${blink ? "|" : " "}`}<br/>мобильные приложения и web‑сервисы</h2>}
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        marginTop: 110,
        [theme.breakpoints.down('sm')]: {
            marginTop: 10
        },
    },
    h1: {
        margin: 0,
        fontWeight: 'bold',
        fontSize: 60,
        [theme.breakpoints.down('sm')]: {
            fontSize: 40
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: 25
        }
    },
    h2: {
        margin: 0,
        marginTop: 50,
        fontWeight: 400,
        fontSize: 25,
        [theme.breakpoints.down('sm')]: {
            fontSize: 20,
            marginTop: 30
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: 18,
            marginTop: 15
        }
    },
  }));

export default Block1Header;
