
import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {sendMail} from '../network/fetchApi'
import { useSnackbar } from 'notistack';
import MuiPhoneNumber from 'material-ui-phone-number'

const Block2 = (props) => {
    const {forwardedRef} = props;
    const {container, h2, form, submitButton} = useStyles();
    const { enqueueSnackbar } = useSnackbar();

    const [name, setName] = useState('');
    const [mail, setMail] = useState('');
    const [phone, setPhone] = useState('');
    const [about, setAbout] = useState('');

    const onNameChange = (event) => {
        setName(event.target.value)
    }

    const onMailChange = (event) => {
        setMail(event.target.value)
    }

    const onPhoneChange = (value) => {
        setPhone(value)
    }

    const onAboutChange = (event) => {
        setAbout(event.target.value)
    }

    const onSendMail = () => {
        const body = {
            subject: "Новый заказ с сайта",
            body: "Имя: " + name + "\n" 
            + "Почта: " + mail + "\n" 
            + "Телефон: " + phone + "\n" 
            + "О проекте: " + about
        }
        sendMail(body, (responseJson) => {
            if(responseJson.status == 200){
                setName("")
                setMail("")
                setPhone("")
                setAbout("")
                enqueueSnackbar('Спасибо, что обратились к нам! С Вами скоро свяжется наш представитель.', { variant: 'success' });
            }
        })
    }

    return (
        <div ref={forwardedRef} className={container}>
            <h2 className={h2}>Создадим нетипичный проект вместе</h2>
            <div className={form}>
                <TextField 
                    id="standard-basic"
                    required
                    label="Имя"
                    style={{width: '100%', marginLeft: 20, marginRight: 20}}
                    color="primary"
                    onChange={onNameChange}
                    value={name}
                />
                <TextField 
                    id="standard-basic"
                    required
                    label="Почта"
                    style={{width: '100%', marginLeft: 20, marginRight: 20}}
                    onChange={onMailChange}
                    value={mail}
                />
                <MuiPhoneNumber 
                    id="standard-basic"
                    label="Телефон"
                    style={{width: '100%', marginLeft: 20, marginRight: 20}}
                    onChange={onPhoneChange}
                    value={phone}
                    defaultCountry={'ru'} 
                />
                <TextField 
                    id="standard-basic"
                    label="Расскажите о проекте"
                    style={{width: '100%', marginLeft: 20, marginRight: 20}}
                    onChange={onAboutChange}
                    value={about}
                    multiline
                    rows={8}
                />
                <button onClick={onSendMail} className={submitButton}>Отправить</button>
            </div>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        marginTop: 60,
    },
    h2: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    form: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: '0px 3px 6px rgba(0,0,0,0.16)',
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        padding: 10,
        paddingBottom: 20,
        borderRadius: 5,
        '& > *': {
            margin: theme.spacing(1),
            width: '50px',
        },
        '& label.Mui-focused': {
            color: theme.palette.text.primary,
          },
        '& .MuiInput-underline:after': {
            borderBottomColor: '#FFE762',
        }
    },
    submitButton: {
        width: '100%',
        backgroundColor: theme.palette.button.active,
        color: theme.palette.button.text,
        fontSize: 15,
        fontWeight: 'bold',
        height: 40,
        borderRadius: 20,
        marginTop: 20,
        boxShadow: 'none'
    }
}));

export default Block2;
