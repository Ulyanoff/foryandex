import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import {AppleIcon, AndroidIcon, WebIcon} from '.'

const StatPlatforms = (props) => {
    const {isAndroid, isIOS, isWeb, color} = props;

    const {container, h3, iconsContainer, icon} = useStyles();

    return (
        <div className={container}>
            <div className={iconsContainer}>
                {isIOS && <AppleIcon fill={color} className={icon}/>}
                {isAndroid && <AndroidIcon fill={color} className={icon}/>}
                {isWeb && <WebIcon fill={color} className={icon}/>}
            </div>
            <h3 className={h3} style={{color}}>платформы</h3>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    iconsContainer: {
        flexDirection: 'row',
    },
    h3: {
        margin: 0,
        fontWeight: 400,
        fontSize: 20,
        [theme.breakpoints.down('sm')]: {
            fontSize: 18,
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: 16,
            marginTop: 5,
        }
    },
    icon: {
        marginRight: 15,
        width: 27,
        [theme.breakpoints.down('xs')]: {
            width: 24,
        }
    }
}));

export default StatPlatforms;
