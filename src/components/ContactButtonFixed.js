import { makeStyles, useTheme } from '@material-ui/core/styles';
import {PhoneIcon} from '.'
import Box from '@material-ui/core/Box';

const ContactButtonFixed = (props) => {
    const {onClick} = props;
    const {container, icon} = useStyles();
    const theme = useTheme();

    return (
        <Box display={{ xs: 'block', md: 'none' }}>
            <button onClick={onClick} className={container}>
                <PhoneIcon fill={theme.palette.background.default} className={icon}/>
            </button>
        </Box>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        backgroundColor: 'red',
        position: 'fixed',
        right: 15,
        bottom: 15,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: theme.palette.button.active,
        padding: 10
    },
    icon: {

    }
}));

export default ContactButtonFixed;
