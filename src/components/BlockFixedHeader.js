import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const BlockFixedHeader = (props) => {
    const {children, style} = props;
    const {container, sticky} = useStyles();

    return (
        <Box className={container} display={{ xs: 'none', md: 'block' }}>
            <div className={sticky}>
                {children}
            </div>
        </Box>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        padding: 25,
        // display: 'flex',
        // justifyContent: 'center',
        textAlign: 'center',
        [theme.breakpoints.down('lg')]: {
            flex: 1
        }
    },
    sticky: {
        position: 'sticky', 
        top: 25
    },
}));

export default BlockFixedHeader;
