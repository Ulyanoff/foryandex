const PhoneIcon = (props) => {
    const {fill, className} = props;

    return (
        <svg className={className} fill={fill} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.92 28.92">
            <path id="Icon_metro-phone" data-name="Icon metro-phone" d="M10.283,21.208c1.928,1.928,1.928,3.856,3.856,3.856s3.856-1.928,5.784-3.856,3.856-3.856,3.856-5.784-1.928-1.928-3.856-3.856,3.856-7.712,5.784-7.712S31.491,9.64,31.491,9.64c0,3.856-3.961,11.674-7.712,15.424S12.211,32.776,8.355,32.776c0,0-5.784-3.856-5.784-5.784s5.784-7.712,7.712-5.784Z" transform="translate(-2.571 -3.856)"/>
        </svg>
    )
}

export default PhoneIcon;
