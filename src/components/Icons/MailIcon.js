const MailIcon = (props) => {
    const {fill, className} = props;

    return (
        <svg className={className} fill={fill} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 24">
            <path id="Icon_metro-mail" data-name="Icon metro-mail" d="M32.141,7.712h-24a2.986,2.986,0,0,0-2.985,3l-.015,18a3,3,0,0,0,3,3h24a3,3,0,0,0,3-3v-18a3,3,0,0,0-3-3Zm0,6-12,7.5-12-7.5v-3l12,7.5,12-7.5v3Z" transform="translate(-5.141 -7.712)"/>
        </svg>
    )
}

export default MailIcon;
