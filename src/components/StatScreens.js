import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import {AppleIcon, AndroidIcon, WebIcon} from '.'

const StatScreens = (props) => {
    const {text, color} = props;

    const {root, h3, h2} = useStyles();

    return (
        <div className={root}>
            <h2 className={h2} style={{color}}>{text}</h2>
            <h3 className={h3} style={{color}}>экранов</h3>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    h3: {
        margin: 0,
        fontWeight: 400,
        fontSize: 20,
        [theme.breakpoints.down('sm')]: {
            fontSize: 18,
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: 16,
            marginTop: 5,
        }
    },
    h2: {
        margin: 0,
        fontWeight: 'bold',
        fontSize: 35,
        // [theme.breakpoints.down('sm')]: {
        //     fontSize: 18,
        // },
        // [theme.breakpoints.down('xs')]: {
        //     fontSize: 16,
        //     marginTop: 5,
        // }
    }
}));

export default StatScreens;
