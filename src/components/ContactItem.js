import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import px2vw from '../utils/px2vw'

const ContactItem = (props) => {
    const {children, title, text, justify} = props;
    const {container, containerIcon, h2} = useStyles();

    return (
        <Grid container 
            sm
            direction='row'
            justify={justify}
            alignItems="center" 
            wrap='nowrap'
            className={container}
        >
            <Grid item>
                <div className={containerIcon}>
                    {children}
                </div>
            </Grid> 
            <Grid item >
                <h2 className={h2}><b>{title}:</b> {text}</h2>
            </Grid> 
        </Grid>  
    )
}

const useStyles = makeStyles((theme) => ({
    container: {
        // padding: 10
        marginTop: 10
    },
    containerIcon: {
        width: 50,
        height: 50,
        borderRadius: 25,
        border: 'solid',
        borderWidth: 2,
        borderColor: theme.palette.text.primary,
        padding: 10,
        marginRight: 25,
        [theme.breakpoints.down('sm')]: {
            width: 40,
            height: 40,
            padding: 8,
        }
    },
    h2: {
        margin: 0,
        fontWeight: 400,
        fontSize: 20,
        [theme.breakpoints.down('xs')]: {
            fontSize: 16,
        }
    }
}));

export default ContactItem;
