import { makeStyles } from '@material-ui/core/styles';
import px2vw from '../utils/px2vw'

const ContactButton = (props) => {
    const {onClick} = props;
    const {container} = useStyles();

    return (
        <button onClick={onClick} className={container}>Связаться</button>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        width: '100%',
        maxWidth: 110,
        maxHeight: 40,
        padding: 10,
        marginBottom: 20,
        borderRadius: 20,
        backgroundColor: theme.palette.button.active,
        color: theme.palette.button.text,
        fontSize: 15,
        fontWeight: 'bold',
    }
}));

export default ContactButton;
