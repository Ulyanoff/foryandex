import { makeStyles, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {ContactItem, PhoneIcon, MailIcon} from '.'


const Footer = (props) => {
    const {container, h2, icon} = useStyles();
    const theme = useTheme();
    return (
        <div className={container}>
            <h2 className={h2}>Контакты</h2>
            <Grid container>
                <ContactItem title={'Почта'} text={'info@untypical.ru'} justify='flex-start'>
                    <MailIcon fill={theme.palette.text.primary} className={icon}/>
                </ContactItem>
                <ContactItem title={'Телефон'} text={'+7 (903) 229-24-58'} justify='flex-start'>
                    <PhoneIcon fill={theme.palette.text.primary}/>
                </ContactItem>
            </Grid>
        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        marginTop: 60,
        marginBottom: 20
    },
    h2: {
        fontSize: 30,
        fontWeight: 'bold',
        margin: 0,
        marginBottom: 10
    },
    icon: {
        marginTop:3
    }
}));

export default Footer;
