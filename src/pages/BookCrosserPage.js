import React, {useRef, useState, useEffect} from "react";
import { makeStyles } from '@material-ui/core/styles';
import ym from 'react-yandex-metrika';

import {LogoPanel, BlockFixedHeader, ContactButton, DarkSwitch, ContactButtonFixed, Footer,
    Block2, BlockPortfolio} from '../components'

import screenshot1 from '../assets/img/bookcrosserscreen1.png'
import screenshot2 from '../assets/img/bookcrosserscreen2.png'
import screenshot3 from '../assets/img/bookcrosserscreen3.png'

const  BookCrosserPage = (props) => {
  const {isDarkTheme, onDarkThemeChange} = props;
  const {root, body, screenshots} = useStyles();
  const block2Ref = useRef(null)
  const dynamicColor = 'white'

  const [logoColor, setLogoColor] = useState(dynamicColor)
  const [switchTextColor, setSwitchTextColor] = useState(dynamicColor)

  const scrollToContact = () => {
    block2Ref.current.scrollIntoView()
  }

  const listenScrollEvent = (event) => {
    if (window.scrollY > 540) {
      setLogoColor("")
    } else {
      setLogoColor(dynamicColor)
    } 

    if (window.scrollY > 470) {
      setSwitchTextColor("")
    }
    else {
      setSwitchTextColor(dynamicColor)
    } 
  }

  useEffect(() => {
    window.addEventListener('scroll', listenScrollEvent);
    ym('hit', '/bookcrosser')

    return () =>
      window.removeEventListener('scroll', listenScrollEvent);
  }, []);

  return (
    <div className={root}>
        <BlockFixedHeader>
            <LogoPanel color={logoColor}/>
        </BlockFixedHeader>
        <div className={body}>
            <BlockPortfolio 
              isIOS isAndroid 
              textColor={dynamicColor}
              screens="25" 
              image={screenshot1}
              title={'BookCrosser'} 
              subTitle={'Социальная сеть для обмена книг по всему миру'} 
              texts={[ 
                'Просмотр книг в своем городе или в любой точке мира на карте;', 
                'Создание меток для книг или шкафов с книгами на карте;',
                'История путешествия книги с геометками и комментариями;',
                'Поиск по названию книг и шкафов;',
                'Сканер QR-кодов для отметки книга на карте.'
                ]}
              isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}
            />
            <img src={screenshot2} className={screenshots}></img>
            <img src={screenshot3} className={screenshots}></img>
            <Block2 forwardedRef={block2Ref}/>
            <Footer/>
        </div>
        <BlockFixedHeader>
            <ContactButton onClick={scrollToContact}/>
            <DarkSwitch color={switchTextColor} checked={isDarkTheme} onChange={onDarkThemeChange} name="checkedA" />
        </BlockFixedHeader>
        <ContactButtonFixed onClick={scrollToContact}/>
        <div style={{zIndex: -1, position: 'absolute', top: 0, left: 0, right: 0, height: 600, backgroundColor: '#DC0000'}}/>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    positon: 'relative',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'strech',
    justifyContent: 'center'
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 1000,
    [theme.breakpoints.down('sm')]: {
      marginLeft: 50,
      marginRight: 50,
      paddingBottom: 60,
    },
    [theme.breakpoints.down('xs')]: {
      marginLeft: 20,
      marginRight: 20,
      paddingBottom: 60,
    }
  },
  screenshots: {
    width: '100%',
    height: 'auto',
    marginTop: 100,
    [theme.breakpoints.down('md')]: {
        marginTop: 50,
    },
  }
}));

export default BookCrosserPage;
