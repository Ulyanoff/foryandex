import React, {useRef, useState, useEffect} from "react";
import { makeStyles } from '@material-ui/core/styles';
import ym from 'react-yandex-metrika';

import {LogoPanel, BlockFixedHeader, ContactButton, DarkSwitch, ContactButtonFixed, Footer,
    Block2, BlockPortfolio} from '../components'

import screenshot1 from '../assets/img/involverscreen1.png'
import screenshot2 from '../assets/img/involverscreen2.png'
import screenshot3 from '../assets/img/involverscreen3.png'

const InvolverPage = (props) => {
  const {isDarkTheme, onDarkThemeChange} = props;
  const {root, body, screenshots} = useStyles();
  const block2Ref = useRef(null)
  const dynamicColor = 'black'

  const [logoColor, setLogoColor] = useState(dynamicColor)
  const [switchTextColor, setSwitchTextColor] = useState(dynamicColor)

  const scrollToContact = () => {
    block2Ref.current.scrollIntoView()
  }

  const listenScrollEvent = (event) => {
    console.log(window.scrollY)
    if (window.scrollY > 540) {
      setLogoColor("")
    }
    else {
      setLogoColor(dynamicColor)
    } 

    if (window.scrollY > 470) {
      setSwitchTextColor("")
    }
    else {
      setSwitchTextColor(dynamicColor)
    } 
  }

  useEffect(() => {
    window.addEventListener('scroll', listenScrollEvent);
    ym('hit', '/involver')
  
    return () =>
      window.removeEventListener('scroll', listenScrollEvent);
  }, []);

  return (
    <div className={root}>
        <BlockFixedHeader>
            <LogoPanel color={logoColor}/>
        </BlockFixedHeader>
        <div className={body}>
          <BlockPortfolio 
            isIOS isAndroid 
            screens="20" 
            image={screenshot1}
            textColor={dynamicColor}
            title={'Involver'} 
            subTitle={'Мобильное приложение по организации и проведению обучающих игр для фармацевтических компании'} 
            texts={[ 
                'Создание и модерация обучающих игр через администраторскую панель;', 
                'Личный кабинет пользователя со списком игр;',
                'Выбор персонифициорованного оформления личного кабинета через админскую панель;', 
                'Сбор статистики и аналитика игр;',
                'Автоматическое создание Excel и PDF отчетов.'
            ]}
            isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}
          />
          <img src={screenshot2} className={screenshots}></img>
          <img src={screenshot3} className={screenshots}></img>
          <Block2 forwardedRef={block2Ref}/>
          <Footer/>
        </div>
        <BlockFixedHeader>
            <ContactButton onClick={scrollToContact}/>
            <DarkSwitch color={switchTextColor} checked={isDarkTheme} onChange={onDarkThemeChange} name="checkedA" />
        </BlockFixedHeader>
        <ContactButtonFixed onClick={scrollToContact}/>
        <div style={{zIndex: -1, position: 'absolute', top: 0, left: 0, right: 0, height: 600, backgroundColor: '#FFF', boxShadow: '0px 0px 6px rgba(0,0,0,0.16)'}}/>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    positon: 'relative',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'strech',
    justifyContent: 'center'
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 1000,
    [theme.breakpoints.down('sm')]: {
      marginLeft: 50,
      marginRight: 50,
      paddingBottom: 60,
    },
    [theme.breakpoints.down('xs')]: {
      marginLeft: 20,
      marginRight: 20,
      paddingBottom: 60,
    }
  },
  screenshots: {
    width: '100%',
    height: 'auto',
    marginTop: 100,
    [theme.breakpoints.down('md')]: {
        marginTop: 50,
    },
  }
}));

export default InvolverPage;
