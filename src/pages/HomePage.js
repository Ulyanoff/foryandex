import React, {useEffect, useRef} from "react";
import { makeStyles } from '@material-ui/core/styles';
import ym from 'react-yandex-metrika';

import {LogoPanel, BlockFixedHeader, ContactButton, DarkSwitch, Block1, Block2, ContactButtonFixed, Footer,
  Block3} from '../components'

const  HomePage = (props) => {
  const {isDarkTheme, onDarkThemeChange} = props;
  const {container, body} = useStyles();
  const block2Ref = useRef(null)

  const scrollToContact = () => {
    block2Ref.current.scrollIntoView()
  }

  useEffect(() => {
    ym('hit', '/')
  }, [])

  return (
    <div className={container}>
      <BlockFixedHeader>
        <LogoPanel/>
      </BlockFixedHeader>
      <div className={body}>
        <Block1 isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
        <Block3/>
        <Block2 forwardedRef={block2Ref}/>
        <Footer/>
      </div>
      <BlockFixedHeader>
        <ContactButton onClick={scrollToContact}/>
        <DarkSwitch checked={isDarkTheme} onChange={onDarkThemeChange} name="checkedA" />
      </BlockFixedHeader>
      <ContactButtonFixed onClick={scrollToContact}/>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'strech',
    justifyContent: 'center'
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 1000,
    [theme.breakpoints.down('sm')]: {
      marginLeft: 50,
      marginRight: 50,
      paddingBottom: 60,
    },
    [theme.breakpoints.down('xs')]: {
      marginLeft: 20,
      marginRight: 20,
      paddingBottom: 60,
    }
  }
}));

export default HomePage;
