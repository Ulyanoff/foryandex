import {SENDMAIL} from './url'

const fetchApi = (url, method, headers, body, response) => {
    var getRequestInfo = {
        method: method,
        headers: headers,
        credentials: 'include',
    }

    var postRequestInfo = {
        method: method,
        headers: headers,
        body: JSON.stringify(body),
        credentials: 'include'
    }

    fetch(url, method == 'POST' || body != null?postRequestInfo:getRequestInfo)
    .then((response) => 
    {
        // console.log(" - response headers: " + JSON.stringify(response.headers));
        // for (var pair of response.headers.entries()) {
        //     console.log(pair[0]+ ': '+ pair[1]);
        //   }
        return Promise.all([response.status, response.json(), response.headers.get('Set-Cookie')]) 
    })
    .then((res) => ({status: res[0], body: res[1], cookie: res[2]}))  
    .then((responseJson) => {
        console.log("\n" + method + "(" + url + "):");
        console.log(" - headers: " + JSON.stringify(headers));
        console.log(" - body: " + JSON.stringify(body));
        console.log(" - responseJson: " + JSON.stringify(responseJson));
        response(responseJson);
        // setCookie(responseJson.cookie);
    })
    .catch((error) => {
        console.error("Error("+ url + "): " + error);
    });
}


export const sendMail = (body, response) => {
    var headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
    fetchApi(SENDMAIL, 'POST', headers, body, response)
}