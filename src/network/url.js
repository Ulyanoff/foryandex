const protocolHttps = "https://"
const host = 'untypical.ru'
const postfix = "/api"

const APIHOST = protocolHttps + host + postfix

export const SENDMAIL = APIHOST + '/mail-sender/feedback'
