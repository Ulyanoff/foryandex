
import React, {useEffect, useState} from "react";
import {HomePage, BookCrosserPage, InvolverPage, VdelePage} from './pages'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles } from '@material-ui/core/styles';
import { SnackbarProvider } from 'notistack';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ym from 'react-yandex-metrika';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import ScrollToTop from './utils/ScrollToTop'

const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    background: {
      default: '#101010',
      paper: '#1C1C1C'
    },
    text: {
      primary: '#fff'
    },
    button: {
      text: '#101010',
      active: '#FFE762'
    }
  }
});

const lightTheme = createMuiTheme({
  palette: {
    type: 'light',
    text: {
      primary: '#101010'
    },
    button: {
      text: '#fff',
      active: '#101010'
    }
  }
});

const App = () => {
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
    const [isDarkTheme, setIsDarkTheme] = useState(false)
    const {root} = useStyles();
      
    const onDarkThemeChange = (event) => {
      setIsDarkTheme(event.target.checked);
      localStorage.setItem('isDarkTheme', event.target.checked);
    };
  
    useEffect(() => {
      const storedIsDarkTheme = localStorage.getItem('isDarkTheme');
      console.log("storedIsDarkTheme: " + storedIsDarkTheme)
      if(storedIsDarkTheme === null) {
        console.log("storedIsDarkTheme === null: " + prefersDarkMode)
        setIsDarkTheme(prefersDarkMode)
      }
      else {
        console.log("storedIsDarkTheme !== null: " + storedIsDarkTheme)
        setIsDarkTheme(JSON.parse(storedIsDarkTheme))
      }
    }, [prefersDarkMode])

    useEffect(() => {
      ym('reachGoal', 'darkTheme', {isDarkTheme: isDarkTheme});
    }, [isDarkTheme])

    return (
      <SnackbarProvider maxSnack={3}>
        <ThemeProvider theme={isDarkTheme?darkTheme:lightTheme}>
        <CssBaseline />
          <Router>
            <ScrollToTop />
            <Switch>
              <Route exact path="/">
                <HomePage isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
              </Route>
              <Route exact path="/bookcrosser">
                <BookCrosserPage isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
              </Route>
              <Route exact path="/involver">
                <InvolverPage isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
              </Route>
              <Route exact path="/vdele">
                <VdelePage isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
              </Route>
              <Route path="*">
                <HomePage isDarkTheme={isDarkTheme} onDarkThemeChange={onDarkThemeChange}/>
              </Route>
            </Switch>
          </Router>
        </ThemeProvider>
      </SnackbarProvider>
    );
}

const useStyles = makeStyles((theme) => ({
  root: {

  }
}));

export default App;
